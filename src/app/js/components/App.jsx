import { hot } from "react-hot-loader/root";
import React from "react";
import "../../styles/App.css";
import { remote } from "electron";
const fs = require("fs");

import bigInt from 'big-integer'

import {
	Container,
	Row,
	Col,
	Form,
	Button,
	InputGroup,
	FormControl,
	ButtonToolbar,
	DropdownButton,
	Dropdown
} from "react-bootstrap";

import NewLFSR from "../cipher/NewLFSR";
import NewGeffe from "../cipher/NewGeffe";

const upToNBytes = string => {
	return "0".repeat(16 - string.length) + string;
}

const formatKey = str => {
	const allowedDigits = '01'
	const real = str.split('').reduce((acc, digit) => {
		return acc += allowedDigits.indexOf(digit) !== -1 ? digit : ''
	}, '').slice(-64)

	return real ? real : '0'
}
import * as Utils from '../rsa/Utils'
import RSA from '../rsa/RSA'

// let rsa
class App extends React.Component {
	constructor() {
		super();

		this.state = {
			p: "107",
			q: "103",
			r: "",
			eulers: "",
			public: {
				e: ""
			},
			private: {
				d: "101"
			},

			plaintext: '',
			ciphertext: '',
			inputFilePath: '',
			outputFilePath: '',

			readBuffer: null,
			encodedBuffer: null,
			currentMode: 0
		};

		this.modes = ['Encoding mode', 'Decoding mode']
	}

	handleInputFileChoose() {
		let result = remote.dialog.showOpenDialogSync({
			properties: ["openFile"]
		});
		let file = result ? result[0] : "";

		this.handleRead(file);
		this.setState({ inputFilePath: file });

		
	}

	handleOutputFileChoose() {
		let result = remote.dialog.showOpenDialogSync({
			properties: ["openFile"]
		});
		let file = result ? result[0] : "";
		this.setState({ outputFilePath: file });
	}

	handleRead(file) {
		const fd = fs.openSync(file, 'r')
		const stats = fs.fstatSync(fd)

		let bufferSize = stats.size,
			readBuffer = Buffer.alloc(bufferSize)

		fs.readSync(fd, readBuffer, 0, bufferSize, 0)
		fs.closeSync(fd)

		console.log(readBuffer)

		this.setState({readBuffer})
		this.writePlaintextFromBuffer(readBuffer, this.state.currentMode === 0 ? 1 : 4)
	}

	handleSave() {
		const { outputFilePath } = this.state;

		let writeBuffer
		if (this.state.currentMode === 0) {
			writeBuffer = this.state.encodedBuffer
		} else {
			writeBuffer = this.state.decodedBuffer
		}

		const fd = fs.openSync(outputFilePath, 'w')

		let bufferSize = writeBuffer.length

		fs.writeSync(fd, writeBuffer, 0, bufferSize, 0)
		fs.closeSync(fd)

		console.log(writeBuffer)
	}

	areAllValid() {
		const a = Utils.validatePQ(this.state.p, this.state.q)
		const b = Utils.validatePrivateKey(this.state.private.d)
		return a && b
	}

	generateKeys () {
		this.rsa = new RSA(this.state.p, this.state.q, this.state.private.d);

		this.setState({
			r: this.rsa.multiplication.toString(),
			eulers: this.rsa.eulers.toString(),
			public: { e: this.rsa.publicKey.toString() },
		});
	}

	writePlaintextFromBuffer (buffer, chunkSize = 1) {
		let offset = 0, numbersPrinted = 0, plaintext = ''

		while (offset < buffer.length) {
			if (numbersPrinted < 64) {
				switch (chunkSize) {
					case 1:
						plaintext += buffer.readUInt8(offset) + ' '
						break
					case 4:
						plaintext += buffer.readUInt32BE(offset) + ' '
						break
				}

				offset += chunkSize
				numbersPrinted++
			} else {
				break
			}
		}

		this.setState({plaintext})
	}

	writeCiphertextFromBuffer (buffer, chunkSize = 4) {
		let offset = 0, numbersPrinted = 0, ciphertext = ''

		while (offset < buffer.length) {
			if (numbersPrinted < 64) {
				switch (chunkSize) {
					case 1:
						ciphertext += buffer.readUInt8(offset) + ' '
						break
					case 4:
						ciphertext += buffer.readUInt32BE(offset) + ' '
						break
				}

				offset += chunkSize
				numbersPrinted++
			} else {
				break
			}
		}

		this.setState({ciphertext})
	}

	handleEncode() {
		const {rsa} = this

		const chunkSize = Utils.getChunkSize(rsa.multiplication)
		const {readBuffer} = this.state

		let bufferSize = readBuffer.length * chunkSize,
			encodedBuffer = Buffer.alloc(bufferSize)


		let offset = 0
		for (let byte of readBuffer) {
			const encoded = rsa.encodeByte(byte).toJSNumber()
			switch (chunkSize) {
				case 4:
					encodedBuffer.writeUInt32BE(encoded, offset)
					break
			}
			offset += chunkSize
		}

		console.log(encodedBuffer)
		this.setState({encodedBuffer});

		this.writeCiphertextFromBuffer(encodedBuffer, this.state.currentMode === 0 ? 4 : 1)	
	}

	handleDecode () {
		const {rsa} = this

		const chunkSize = Utils.getChunkSize(rsa.multiplication)
		const {readBuffer} = this.state

		let bufferSize = Math.round(readBuffer.length / chunkSize),
			decodedBuffer = Buffer.alloc(bufferSize)

		let readOffset = 0, writeOffset = 0
		while (readOffset < readBuffer.length) {
			switch (chunkSize) {
				case 4:
					const encoded = readBuffer.readUInt32BE(readOffset)
					decodedBuffer.writeUInt8(rsa.decodeByte(encoded).toJSNumber(), writeOffset)
					break
			}
			writeOffset += 1
			readOffset += chunkSize
		}


		console.log(decodedBuffer)
		this.setState({decodedBuffer});

		this.writeCiphertextFromBuffer(decodedBuffer, this.state.currentMode === 0 ? 4 : 1)	
	}

	onModeChange (e) {
		const currentMode = this.modes.findIndex(mode => mode === e.target.value);
		this.setState({currentMode}, () => console.log(this.state.currentMode))

	}

	render() {
		return (
			<Container>
				<Row>
					<Col>
						<Form>
							<Form.Control
								onChange={e => this.onModeChange(e)}
								value={this.modes[this.state.currentMode]}
								as="select"
							>
								{this.modes.map((mode, index) => (
									<option key={index} value={mode}>{mode}</option>
								))}
							</Form.Control>
						</Form>
					</Col>
				</Row>
				<Row>
					<Col md={6}>
						<Form>
							<Form.Group controlId="inputTextArea">
								<Form.Label>
									Input text ({"<"}= 64 bytes)
								</Form.Label>
								<Form.Control
									defaultValue={this.state.plaintext}
									as="textarea"
									rows="12"
								/>
							</Form.Group>
						</Form>
					</Col>
					<Col md={6}>
						<Form>
							<Form.Group controlId="outputTextArea">
								<Form.Label>
									Ouput text ({"<"}= 64 bytes)
								</Form.Label>
								<Form.Control
									defaultValue={this.state.ciphertext}
									as="textarea"
									rows="12"
								/>
							</Form.Group>
						</Form>
					</Col>
				</Row>

				<Row>
					<Col>
						<Form>
							<Form.Group controlId="keyFiles">
								<InputGroup className="mb-3">
									<InputGroup.Prepend>
										<Button
											onClick={() =>
												this.handleInputFileChoose()
											}
											variant="outline-primary"
										>
											Choose input file
										</Button>
									</InputGroup.Prepend>
									<FormControl
										readOnly
										aria-describedby="basic-addon1"
										value={this.state.inputFilePath}
									/>
								</InputGroup>
								<InputGroup className="mb-3">
									<InputGroup.Prepend>
										<Button
											onClick={() =>
												this.handleOutputFileChoose()
											}
											variant="outline-primary"
										>
											Choose output file
										</Button>
										<Button
											onClick={() => this.handleSave()}
											disabled={
												this.state.outputFilePath == ""
											}
											variant="outline-success"
										>
											Save
										</Button>
									</InputGroup.Prepend>
									<FormControl
										readOnly
										aria-describedby="basic-addon1"
										value={this.state.outputFilePath}
									/>
								</InputGroup>
							</Form.Group>
						</Form>
					</Col>
				</Row>

				<Row>
					<Col>
						<Form>
							<Form.Group controlId="lonelyRegister">
								<Form.Label>p</Form.Label>
								<InputGroup className="mb-3">
									<Form.Control
										type="text"
										placeholder="Enter some digits here"
										value={this.state.p}
										onChange={e =>
											this.setState({
												p: e.target.value
											})
										}
									/>
								</InputGroup>

								<Form.Label>q</Form.Label>
								<InputGroup className="mb-3">
									<Form.Control
										type="text"
										placeholder="Enter some digits here"
										value={this.state.q}
										onChange={e =>
											this.setState({
												q: e.target.value
											})
										}
									/>
								</InputGroup>

								<Form.Label>d (private key)</Form.Label>
								<InputGroup className="mb-3">
									<Form.Control
										type="text"
										placeholder="Enter some digits here"
										value={this.state.private.d}
										onChange={e =>
											this.setState({
												private: { d: e.target.value }
											})
										}
									/>
								</InputGroup>

								<InputGroup className="mb-3">
									<Button
										variant="success"
										onClick={() => this.generateKeys()}
										disabled={!this.areAllValid()}
									>
										Generate keys
									</Button>
								</InputGroup>

								<InputGroup className="mb-3">
									<Button
										variant="primary"
										onClick={() => this.handleEncode()}
									>
										Encode
									</Button>
								</InputGroup>

								<InputGroup className="mb-3">
									<Button
										variant="primary"
										onClick={() => this.handleDecode()}
									>
										Decode
									</Button>
								</InputGroup>
							</Form.Group>
						</Form>
					</Col>
				</Row>

				<Row>
					<Col>
						<Form>
							<Form.Group controlId="lonelyRegister">
								<Form.Label>r (production of p & q)</Form.Label>
								<InputGroup className="mb-3">
									<Form.Control
										readOnly
										type="text"
										placeholder=""
										defaultValue={this.state.r}
									/>
								</InputGroup>

								<Form.Label>
									ȹ (Euler's function value)
								</Form.Label>
								<InputGroup className="mb-3">
									<Form.Control
										readOnly
										type="text"
										placeholder=""
										defaultValue={this.state.eulers}
									/>
								</InputGroup>

								<Form.Label>e (public key)</Form.Label>
								<InputGroup className="mb-3">
									<Form.Control
										readOnly
										type="text"
										placeholder=""
										defaultValue={this.state.public.e}
									/>
								</InputGroup>
							</Form.Group>
						</Form>
					</Col>
				</Row>
			</Container>
		);
	}
}

export default hot(App);
