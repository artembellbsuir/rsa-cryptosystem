class NewLFSR {

    constructor(state, powers) {
        this.high = ((state.length > 32) ? parseInt(state.slice(0, -32), 2) : 0) << 0
        this.low = parseInt(state.slice(-32), 2) << 0

        this.maxPower = Math.max(...powers)
        this.masks = powers.map(power => 2 ** (power - 1))
        this.highestBitMask = 2 ** (this.maxPower - 1)

        this.MAX = 2 ** 32
        this.PMAX = 2 ** 31

    }

    getNextKey() {
        let key = 0
        for (let i = 1; i <= 8; i++) {
            key = key << 1 | this.getBitAt(this.highestBitMask)
            this.shiftRegister()
        }

        return key
    }

    shiftRegister() {
        let bits = this.masks.map(mask => this.getBitAt(mask))
        let resultBit = bits.reduce((next, prev) => next ^ prev)
        let firstHighBit = (this.low < 0) ? 1 : 0

        this.high = firstHighBit === 1 ? (this.high << 1) | 1 : this.high << 1
        this.low = resultBit === 1 ? (this.low << 1) | 1 : this.low << 1

        // const h = new Uint32Array([this.high])
        // const l = new Uint32Array([this.low])
        // const s = h & l

    }

    getBitAt(mask) {
        if (mask >= this.MAX) {
            return Number(Boolean(this.high & (mask / this.MAX)));
        } else {
            return Number(Boolean(this.low & mask));
        }
    }
}

export default NewLFSR