import LFSR from './NewLFSR'

class NewGeffe {
    constructor(states, powers) {

        this.registers = [
            new LFSR(states[0], powers[0]),
            new LFSR(states[1], powers[1]),
            new LFSR(states[2], powers[2])
        ]

    }

    // getkey(i) {
    //     return 
    // }

    getNextKey() {
        let keys = [];

        for (let i = 0; i < 3; i++) {
            keys[i] = 0
            keys[i] = this.registers[i].getNextKey()
        }


        return [
            (keys[0] & keys[1]) | (~keys[0] & keys[2]),
            keys[0], keys[1], keys[2]
        ]
        
    }
}

export default NewGeffe