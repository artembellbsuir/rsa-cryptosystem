let keys, registers, powers, maxPowers, maxs, masks, poppedBits = [], geffeKey = new Uint8Array([0])

export const initGeffeRegisters = (initialStates, allPowers) => {
    registers = initialStates
    powers = allPowers
    keys = new Uint8Array([0, 0, 0])
    maxPowers = powers.map(powerArr => Math.max(...powerArr))
    maxs = maxPowers.map((power, i) => 2 ** (maxPowers[i] - 1))
    masks = powers.map((powerArr, i) => powerArr.map(power => 2 ** (maxPowers[i] - power)))
}

const calcNextKey = (k) => {

    keys[k] = keys[k] << 1
    const iff = (BigInt(registers[k]) & BigInt(1)) === BigInt(1)
    keys[k] = iff ? keys[k] | 1 : keys[k]

    const bits = []
    for(let mask of masks[k]) {
        const res = BigInt(registers[k]) & BigInt(mask) 
        if (res === BigInt(mask)) {
            bits.push(1)
        } else {
            bits.push(0)
        }
    }

    // const bits = masks[k].map(int => ((registers[k] & int) === int) ? 1 : 0)
    // const xors = masks[k].map(int => ((registers[k] & int) === int) ? 1 : 0).reduce((prev, next) => prev ^ next)
    const xors = bits.reduce((prev, next) => prev ^ next)

    registers[k] = xors === 1 
        ? ((BigInt(registers[k]) >> BigInt(1)) | BigInt(maxs[k])) 
        : (BigInt(registers[k]) >> BigInt(1))

    // registers[k] = masks[k]
    //     .map(int => (registers[k] & int === int) ? 1 : 0)
    //     .reduce((prev, next) => prev ^ next) === 1 ? ((registers[k] >>> 1) | maxs[k]) : registers[k] >>> 1
    
}

export const calcGeffeKey = () => {
    for(let j = 0; j <= 2; j++) {
        for(let i = 1; i <= 8; i++) {
            if(j == 1) {
                // console.log('0'.repeat(36 - registers[1].toString(2).length) + registers[1].toString(2))
                // console.log('0'.repeat(36 - keys[1].toString(2).length) + keys[1].toString(2))
            }
            calcNextKey(j)

            
        }
        
    }


    console.log(
        // '0'.repeat(8 - keys[0].toString(2).length) + keys[0].toString(2),
        // '0'.repeat(8 - keys[1].toString(2).length) + keys[1].toString(2)
        // '0'.repeat(8 - keys[2].toString(2).length) + keys[2].toString(2)
    )
    geffeKey[0] = (keys[0] & keys[1]) | (~keys[0] & keys[2])
    
    return geffeKey[0]
}