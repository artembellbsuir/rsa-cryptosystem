import * as utils from './Utils'
import bigInt from 'big-integer'

class RSA {
    constructor (p, q, d) {
        this.p = bigInt(p)
        this.q = bigInt(q)
        this.d = bigInt(d)

        this.r = this.p.multiply(this.q)
        this.eulersValue = utils.eilersFunction(this.p, this.q)

        const ext = utils.extendedEuclid(this.eulersValue, this.d) 
        this.e = ext < 0 ? ext.plus(this.eulersValue) : ext
    }

    encodeByte (byte) {
        const c = utils.fastExponentiation(byte, this.e, this.r)
        return c 
    }

    decodeByte (byte) {
        const c = utils.fastExponentiation(byte, this.d, this.r)
        return c 
    }

    get eulers () {
        return this.eulersValue
    }

    get multiplication () {
        return this.r
    }

    get publicKey () {
        return this.e
    }    
}

export default RSA