import bigInt from 'big-integer'

export const isPrime = (number) => {
    if (number == 2) {
        return true
    }

    for (let i = 0; i < 100; i++) {
        const a = getRandomRange(3, number - 1)
        if (gcd(a, number) != 1)
            return false;
        if (fastExponentiation(a, number - 1, number) !== 1) {
            return false
        }
    }

    return true
}

export const getRandomRange = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export const gcd = (a, b) => {
    return b === 0 ? a : gcd(b, a % b)
}

export const eilersFunction = (p, q) => {
    return p.minus(bigInt.one).multiply(q.minus(bigInt.one))
}

export const extendedEuclid = (a, b) => {
    let d0 = bigInt(a),
        d1 = bigInt(b),
        x0 = bigInt.one,
        x1 = bigInt.zero,
        y0 = bigInt.zero,
        y1 = bigInt.one

    while (d1.greater(bigInt.one)) {
        let q = d0.divide(d1),
            d2 = d0.mod(d1),
            x2 = x0.minus(q.multiply(x1)),
            y2 = y0.minus(q.multiply(y1))

        d0 = d1
        d1 = d2
        x0 = x1
        x1 = x2
        y0 = y1
        y1 = y2
    }

    return y1
}

export const extendedGcd = (a, b) => {
    if (a === 0) {
        let x = 0,
            y = 1
        return [x, y, b]
    }

    const [x1, y1, d] = extendedGcd(b % a, a),
        x = y1 - Math.floor(b / a) * x1,
        y = x1

    return [x, y, d]
}

export const fastExponentiation = (base, power, mod) => {
    // let number = base,
    //     exp = power,
    //     x = 1

    // while (exp !== 0) {
    //     while (exp % 2 === 0) {
    //         exp = Math.floor(exp / 2)
    //         number = (number * number) % mod
    //     }

    //     exp--
    //     x = (x * number) % mod
    // }

    return bigInt(base).modPow(power, mod)
}


// validations


const isNumber = str => {
    const numbers = '0123456789'
    for (let char in str) {
        if (numbers.indexOf(char) === -1) {
            return false
        }
    }
    return true
}

export const validatePQ = (p, q) => {
    return isNumber(p) && bigInt(p).isPrime() && isNumber(q) && bigInt(q).isPrime()
}

export const validatePrivateKey = (key, p, q) => {
    // if (!isNumber, )
    // const eilers = eilersFunction(bigInt(p), bigInt(q))
    // return (1 < key < eilers) && (gcd(key, eilers) === 1)
    return true
}

const byteSize = 8
export const getChunkSize = r => {
    // return Math.ceil(Math.log2(r) / byteSize)
    return 4
}