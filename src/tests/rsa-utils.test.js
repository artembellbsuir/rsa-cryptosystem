import * as Utils from '../app/js/rsa/Utils'
import RSA from '../app/js/rsa/RSA'

// it('fast exponentiation', () => {
//     const { fastExponentiation } = Utils

//     expect(fastExponentiation(4, 7, 33)).toBe(16)
//     expect(fastExponentiation(2, 7, 33)).toBe(29)
//     expect(fastExponentiation(21, 7, 33)).toBe(21)

//     expect(fastExponentiation(16, 3, 33)).toBe(4)
//     expect(fastExponentiation(29, 3, 33)).toBe(2)
//     expect(fastExponentiation(21, 3, 33)).toBe(21)

//     // ...
// })

// it('greatest common divisor', () => {
//     const { gcd } = Utils

//     expect(gcd(7920, 594)).toBe(198)
//     expect(gcd(941, 331)).toBe(1)
//     expect(gcd(431, 817)).toBe(1)
//     expect(gcd(8121, 932)).toBe(1)
//     expect(gcd(4444, 338)).toBe(2)
//     expect(gcd(1080, 555)).toBe(15)
//     expect(gcd(6126, 822)).toBe(6)
//     expect(gcd(3908, 292)).toBe(4)
//     expect(gcd(7212, 848)).toBe(4)
//     expect(gcd(9812, 334)).toBe(2)
// })

// it('prime number', () => {
//     const { isPrime } = Utils

//     expect(isPrime(7920)).toBeFalsy()
//     expect(isPrime(9477)).toBeFalsy()
//     expect(isPrime(7593)).toBeFalsy()
//     expect(isPrime(8851)).toBeFalsy()
//     expect(isPrime(6078)).toBeFalsy()
//     expect(isPrime(6078)).toBeFalsy()
//     expect(isPrime(6885)).toBeFalsy()
    
//     expect(isPrime(2)).toBeTruthy()
//     expect(isPrime(5323)).toBeTruthy()
//     expect(isPrime(9601)).toBeTruthy()
//     expect(isPrime(8527)).toBeTruthy()
//     expect(isPrime(7577)).toBeTruthy()
//     expect(isPrime(9973)).toBeTruthy()
// })

it('encrypting/decrypting', () => {
    const { extendedEuclid: gcd1, extendedGcd: gcd2 } = Utils

    const rsa = new RSA(107, 103, 101)

    const testNumbers = [214, 93, 104, 9, 200, 174, 119]

    const encoded = testNumbers.map(num => rsa.encodeByte(num).toJSNumber())
    const ansNumbers = encoded.map(enc => rsa.decodeByte(enc).toJSNumber())

    expect(ansNumbers[0]).toBe(testNumbers[0])
    expect(ansNumbers[1]).toBe(testNumbers[1])
    expect(ansNumbers[2]).toBe(testNumbers[2])
    expect(ansNumbers[3]).toBe(testNumbers[3])
    expect(ansNumbers[4]).toBe(testNumbers[4])
    expect(ansNumbers[5]).toBe(testNumbers[5])
    expect(ansNumbers[6]).toBe(testNumbers[6])
})

