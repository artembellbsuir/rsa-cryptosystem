import LFSR from '../app/js/cipher/NewLFSR'

test('get register bits <= 32', () => {
    const r = new LFSR('011101110111100101010101', [25, 5])

    expect(r.getBitAt(2**0)).toBe(1)
    expect(r.getBitAt(2**1)).toBe(0)
    expect(r.getBitAt(2**3)).toBe(0)
    expect(r.getBitAt(2**4)).toBe(1)
    expect(r.getBitAt(2**5)).toBe(0)
    expect(r.getBitAt(2**6)).toBe(1)
    expect(r.getBitAt(2**7)).toBe(0)
    expect(r.getBitAt(2**8)).toBe(1)

    expect(r.getBitAt(2**21)).toBe(1)
    expect(r.getBitAt(2**22)).toBe(1)
    expect(r.getBitAt(2**23)).toBe(0)
    expect(r.getBitAt(2**24)).toBe(0)
})

test('get register bits > 32', () => {
    const r = new LFSR('100000000000000000000000000000000', [40, 5])

    expect(r.getBitAt(2**32)).toBe(1)
    expect(r.getBitAt(2**31)).toBe(0)
    expect(r.getBitAt(2**33)).toBe(0)
})